export class Address {
  id: string;
  color: string;
  contactNumber: string;
  countrySchema: any;
  defaultAddress: boolean;
  dynamicFields: any;
  firstName: string;
  lastNae: string;
  genericFormat: any;
  genericSchema: any;
  ownerId: string;
}
