import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import {ZwoopWatchlistComponent} from './zwoop-watchlist/zwoop-watchlist.component';
import {AuthComponent} from './auth/auth.component';
const routes: Routes = [
  {path: 'watchlist', component: ZwoopWatchlistComponent},
  {path: 'login', component: AuthComponent},
  {path: '', redirectTo: '/watchlist', pathMatch: 'full'},
]

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
