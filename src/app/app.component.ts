import { Component } from '@angular/core';
import { ZwoopConnectionService } from './services/zwoop-connection.service';
import { CustomerService } from './services/customer.service';
import {Customer} from './customer';
import {Address} from './address';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  customer: Customer;
  constructor(private zwoopConnection: ZwoopConnectionService, private customerService:CustomerService){}

  ngOnInit(){
    this.zwoopConnection.connect();
    this.zwoopConnection.isDisconnected().subscribe(event => {
      console.log('disconnected');
      this.zwoopConnection.connect();
    })
    this.customerService.customer.subscribe(value => {
      this.customer = value;
      console.log(value);
      if (this.customer){
          console.log(this.customer.defaultAddress instanceof Address);
      }

    })
  }
}
