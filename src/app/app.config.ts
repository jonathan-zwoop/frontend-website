import { InjectionToken } from '@angular/core';

export const APP_CONFIG = new InjectionToken('app.config');

export interface AppConfig {
  watchlistPageSize: any;
}

export const ZWOOP_CONFIG: AppConfig = {
  watchlistPageSize: 10
};
