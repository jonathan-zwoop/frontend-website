import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AuthComponent } from './auth/auth.component';

import { ZwoopConnectionService } from './services/zwoop-connection.service';
import { ZwoopWatchlistComponent } from './zwoop-watchlist/zwoop-watchlist.component';
import { ZwoopWatchlistService } from './zwoop-watchlist.service';
import { AuthService } from './auth.service';
import { CustomerService } from './services/customer.service';
import { AddressService } from './services/address.service';

import { AppRoutingModule } from './/app-routing.module';

import {APP_CONFIG, ZWOOP_CONFIG} from './app.config';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ZwoopWatchlistComponent,
    AuthComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [
    ZwoopConnectionService,
    ZwoopWatchlistService,
    AuthService,
    CustomerService,
    { provide: APP_CONFIG, useValue: ZWOOP_CONFIG },
    Title,
    AddressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
