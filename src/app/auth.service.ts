import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {
  private isLoggedin = new BehaviorSubject(false);
  constructor(private http:Http) { }

  public doLogin(username, password){
    var AuthUrl= "https://customer-api-dev.zwoop.xyz/authenticate";
    var CsrfUrl: "https://customer-api-dev.zwoop.xyz/auth/zwoop";
    var LoginUrl: "https://customer-oauth-dev.zwoop.xyz/login";
    this.http.get(AuthUrl).subscribe(data =>{
      this.isLoggedin.next(true);
    }, error => {
      this.http.get(CsrfUrl).subscribe(data=>{
        var elements = <any>document.createElement("html");
        elements.innerHTML = data.text();
        var csrf = elements.getElementsByName("_csrf");
        if (typeof csrf == "undefined"){
          this.isLoggedin.next(true);
        }
        this.http.post(LoginUrl, {_csrf: csrf[0].value, username: username, password: password}).subscribe(data => {
          this.isLoggedin.next(true);
        })
      })
    })
  }
}
