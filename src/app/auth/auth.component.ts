import { Component, OnInit, Input } from '@angular/core';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  @Input() username = "";
  @Input() password = "";
  constructor(private auth:AuthService) { }

  ngOnInit() {
  }
  onSubmit(){
    this.auth.doLogin(this.username, this.password);
  }
}
