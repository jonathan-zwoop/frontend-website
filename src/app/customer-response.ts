import {Customer} from './customer'

export class CustomerResponse {
  detail:{
    resource: Customer,
    requestId: string
  };
}
