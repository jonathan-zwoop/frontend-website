import {Address} from './address'

export class Customer {
  activated: boolean;
  createDate: number;
  defaultAddress: Address; //TODO change to address class
  defaultPaymentMethod: any; //TODO change to payment method class
  email: string;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  userPreferences: string;
  zwoopEmail: string;
  constructor(jsonData:any){
    for (let key in jsonData) {
      this[key] = jsonData[key];
    }
    this.defaultAddress = Object.assign(new Address, jsonData.defaultAddress);
  }
}
