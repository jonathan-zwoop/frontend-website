import { Component, OnInit } from '@angular/core';
import { ZwoopConnectionService } from '../services/zwoop-connection.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private isLoggedin = false;
  private title = "Nay";
  constructor(private zwoopConnectionService: ZwoopConnectionService) { }

  ngOnInit() {
    this.zwoopConnectionService.isConnected().subscribe(event => {
      this.title = "Yay";
    })
  }

}
