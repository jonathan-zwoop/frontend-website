import { Injectable } from '@angular/core';
import {Address} from '../address';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ZwoopConnectionService} from './zwoop-connection.service'
import {APP_CONFIG, ZWOOP_CONFIG} from '../app.config'

@Injectable()
export class AddressService {
  public addresses = new BehaviorSubject<any>(undefined);

  constructor(private zwoopConnection: ZwoopConnectionService) {
    this.getAddressListListener();
    this.zwoopConnection.isConnected().subscribe(event => {
      if (this.addresses.value === undefined){
        this.zwoopConnection.messageZwoop('Customer.getAddressList', [{}]);
      }
    })
  }

  private getAddressListListener(){
    Observable.fromEvent(document, 'zwoopSdkService.Customer.getAddressList').subscribe((message: any) => {
      console.log(message);
      var addresses = {};
      message.detail.resourceList.forEach( (address) => {
        addresses[address.id as string] = address;
      })
      this.addresses.next(addresses);
    })
  }

  public getAddress<Address>(addressId: string){
    return (this.addresses.value[addressId] ? this.addresses.value[addressId] : {});
  }
}
