import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ZwoopConnectionService} from './zwoop-connection.service';
import {APP_CONFIG, ZWOOP_CONFIG} from '../app.config';
import {Customer} from '../customer';
@Injectable()
export class CustomerService {
  public customer = new BehaviorSubject<Customer>(undefined);
  constructor(private zwoopConnection: ZwoopConnectionService) {
    this.getCustomerListener();
    this.zwoopConnection.isConnected().subscribe(event => {
      if (this.customer.value === undefined){
        this.zwoopConnection.messageZwoop('Customer.getCustomer', [{}]);
      }
    })
    console.log("Customer: ", this.customer.value === undefined);
  }

  private getCustomerListener(){
    Observable.fromEvent(document, 'zwoopSdkService.Customer.getCustomer').subscribe((message: any) => {
      console.log(message);
      if (this.zwoopConnection.isRequested(message.detail.requestId)){
        var customer = new Customer(message.detail.resource);
        console.log(customer);
        //customer.defaultAddress = Object.assign(new Address, customer.defaultAddress);
        this.customer.next(customer);
      }
    })
  }

  public updateCustomer(customerData){
    this.zwoopConnection.messageZwoop('Customer.updateCustomer', [{customer: customerData}]);
  }

  private updateCustomerListener(){
    Observable.fromEvent(document, 'zwoopSdkService.Customer.updateCustomer').subscribe((message: any)=>{
      if (typeof message.detail.errorCode != "undefined" && message.detail.errorCode != 200){
        if (this.zwoopConnection.isRequested(message.detail.requestId)){
            //TODO update the error subject and display it
        }
      }else{
        this.customer.next(new Customer(message.detail.resource));
      }
    })
  }
}
