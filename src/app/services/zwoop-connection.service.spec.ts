import { TestBed, inject } from '@angular/core/testing';

import { ZwoopConnectionService } from './services/zwoop-connection.service';

describe('ZwoopConnectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ZwoopConnectionService]
    });
  });

  it('should be created', inject([ZwoopConnectionService], (service: ZwoopConnectionService) => {
    expect(service).toBeTruthy();
  }));


});
