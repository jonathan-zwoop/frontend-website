import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/Rx';

@Injectable()
export class ZwoopConnectionService {
  private requestIds = [];
  public connectionStatus = new BehaviorSubject(false);
  subscription:Subscription;
  public connect(){
    (<any>window).ZwoopCustomerApiSdk.initialize({host:"https://customer-api-dev.zwoop.xyz",apiKey:"12345678",sdkRedirectUri: window.location.href});
    this.zwoopSdkServiceRequested();
    this.oauthFailListener();
  }
  public isConnected(){
    return Observable.fromEvent(document, 'zwoopSdkConnected');
  }

  public isDisconnected(){
    return Observable.fromEvent(document, 'zwoopSdkConnectFailed');
  }

  private zwoopSdkServiceRequested(){
    Observable.fromEvent(document, 'zwoopSdkServiceRequested').subscribe(message =>{
      let requestId = (message as any).detail.requestId;
      this.requestIds.push(requestId);
    })
  }

  private oauthFailListener(){
    return Observable.fromEvent(document, 'zwoopSdkAuthenticateRequired');
  }

  public isRequested(requestId){
    return (this.requestIds.indexOf(requestId) !== -1);
  }

  public messageZwoop(service, params){
    if (this.connectionStatus.getValue()){
      (<any>window).ZwoopCustomerApiSdk.service(service, params);
    }else{
      this.subscription = this.connectionStatus.subscribe(value => {
        console.log(value);
        if (value){
          console.log("is connected");
          (<any>window).ZwoopCustomerApiSdk.service(service, params);
          this.subscription.unsubscribe();
        }
      })
      console.log(this.subscription);
    }
  }
  constructor(){
    console.log('initialize connection singleton');
    this.isConnected().subscribe(x => this.connectionStatus.next(true));
    this.isDisconnected().subscribe(x => this.connectionStatus.next(false));
    this.oauthFailListener().subscribe(x => this.connectionStatus.next(false));
  }
}
