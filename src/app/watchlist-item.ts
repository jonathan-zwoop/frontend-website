export class WatchlistItem {
  addressID: string;
  allAttrs: any[];
  bestPriceResponse: any;
  merchantId: string;
  merchantName: string;
  product: any;
  productDesc: string;
  productId: string;
  quantity: number;
  searchType: string;
  timestamp: number;
  url: string;
  watchListId: string

  constructor(jsonData:any){
    for (let key in jsonData) {
      this[key] = jsonData[key];
    }
  }

}
