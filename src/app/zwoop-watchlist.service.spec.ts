import { TestBed, inject } from '@angular/core/testing';

import { ZwoopWatchlistService } from './zwoop-watchlist.service';
import { ZwoopConnectionService } from './services/zwoop-connection.service';

describe('ZwoopWatchlistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ZwoopWatchlistService, ZwoopConnectionService]
    });
  });

  it('should be created', inject([ZwoopWatchlistService], (service: ZwoopWatchlistService) => {
    expect(service).toBeTruthy();
  }));
});
