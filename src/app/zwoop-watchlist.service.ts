import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {ZwoopConnectionService} from './services/zwoop-connection.service';
import {APP_CONFIG, ZWOOP_CONFIG} from './app.config';
import {WatchlistItem} from './watchlist-item';

@Injectable()
export class ZwoopWatchlistService {
  public watchlist = new Subject<any[]>();
  constructor(private zwoopConnection: ZwoopConnectionService) {
    console.log('initialize watchlist');
    this.searchWatchListEventListener();
  }

  public getWatchlist(page = 0){
    console.log("get watchlist")
    this.zwoopConnection.messageZwoop('WatchList.search', [{
      page: page,
      pageSize: ZWOOP_CONFIG.watchlistPageSize
    }])
  }

  public updateWatchlist(){
    this.zwoopConnection.messageZwoop('WatchList.update',[{

    }])
  }

  private searchWatchListEventListener(){
    Observable.fromEvent(document, 'zwoopSdkService.WatchList.search').subscribe((message: any) => {
      console.log("received watchlist");
        if (this.zwoopConnection.isRequested(message.detail.requestId)){
          var watchlist = [];
          message.detail.resource.forEach(jsonData => {
            watchlist.push(new WatchlistItem(jsonData));
          })
          //TODO instead of replacing the old value, check the old value and append/prepend/edit the array
          this.watchlist.next(watchlist);
        }
    })
  }
}
