import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZwoopWatchlistComponent } from './zwoop-watchlist.component';
import {ZwoopWatchlistService} from '../zwoop-watchlist.service';
import {ZwoopConnectionService} from '../services/zwoop-connection.service';
describe('ZwoopWatchlistComponent', () => {
  let component: ZwoopWatchlistComponent;
  let fixture: ComponentFixture<ZwoopWatchlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZwoopWatchlistComponent ],
      providers: [ZwoopWatchlistService, ZwoopConnectionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZwoopWatchlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
