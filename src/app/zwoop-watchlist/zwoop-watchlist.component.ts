import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {ZwoopWatchlistService} from '../zwoop-watchlist.service';
import {Subscription} from 'rxjs/Subscription';
import {WatchlistItem} from '../watchlist-item';
import {AddressService} from '../services/address.service';

@Component({
  selector: 'app-zwoop-watchlist',
  templateUrl: './zwoop-watchlist.component.html',
  styleUrls: ['./zwoop-watchlist.component.css']
})
export class ZwoopWatchlistComponent implements OnInit {
  subscription:Subscription;
  watchlist: WatchlistItem[];
  currentWatchlistItem: WatchlistItem;
  constructor(private zwoopWatchlistService:ZwoopWatchlistService, private titleService: Title, private addressService: AddressService) { }

  ngOnInit() {
    //wait for addresses to load first
    this.addressService.addresses.subscribe(addresses => {
      if (addresses !== undefined){
        this.getWatchlist();
      }
    })
    this.titleService.setTitle("Zwoop Watchlist");
  }

  getWatchlist(): void{
    this.subscription = this.zwoopWatchlistService.watchlist.subscribe(watchlist => {
      this.watchlist = watchlist;
      console.log(this.watchlist);
      this.subscription.unsubscribe();
      if (this.currentWatchlistItem === undefined){
        this.currentWatchlistItem = this.watchlist[0];
        console.log(this.currentWatchlistItem);
      }
    })
    this.zwoopWatchlistService.getWatchlist();
  }

  editItem(item): void{
    console.log(item);
  }

  setCurrentWatchlistItem(watchlistItem): void{
    this.currentWatchlistItem = watchlistItem;
  }

  getAddress(id): string{
    var address = this.addressService.getAddress(id);
    return (address? address.nickname : "");
  }

}
