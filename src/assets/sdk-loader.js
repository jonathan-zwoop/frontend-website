(function (window, undefined) {
	var document = window.document;

	var sdk = {
		isReady: false,
		host: null,
		apiKey: null,
		sdkRedirectUri: null,
		window: null
	};

	var fireEventUpstream = function (document, eventName, eventDetail) {
		var event = document.createEvent('CustomEvent');
		event.initCustomEvent(eventName, true, true, eventDetail);
		document.dispatchEvent(event);
	};

	var sendEventDownstream = function (eventName, args) {
		console.log('send event downstream = ' + eventName);
		var eventDetail = {};
		Object.assign(eventDetail, args);
		eventDetail.eventName = eventName;
		sdk.window.postMessage(eventDetail, sdk.host);
	};

	var receiveEventDownstream = function (event) {
		console.log(event);

		var origin = event.origin || event.originalEvent.origin;
		if (origin !== sdk.host) {
			return;
		}

		var eventName = event.data.eventName;
		delete event.data.eventName;
		console.log(eventName);
		if (eventName.match(/^zwoop/)) {
			if (eventName === 'zwoopSdkInitialized') {
				sendEventDownstream('init', { host: sdk.host, apiKey: sdk.apiKey, sdkRedirectUri: sdk.sdkRedirectUri });
				return;
			} else if (eventName === 'zwoopSdkAuthenticated') {
				sdk.isReady = true;
			}
			fireEventUpstream(document, eventName, event.data);
		}
	};

	sdk.initialize = function (configs) {
		if (sdk.window) {
			sendEventDownstream('connect');
		} else {
			sdk.host = configs.host;
			sdk.apiKey = configs.apiKey;
			sdk.sdkRedirectUri = configs.sdkRedirectUri;
			console.log(window.location.origin);
			var url = sdk.host + '/static/html/sdk.html?origin=' + window.location.origin;
			var sdkFrame = document.createElement('iframe');
			sdkFrame.id = 'zwoopSdkFrame';
			sdkFrame.src = url;
			var where = document.getElementsByTagName('body')[0];
			where.append(sdkFrame);

			window.addEventListener('message', receiveEventDownstream, false);

			sdk.window = sdkFrame.contentWindow || sdkFrame;
		}
	};

	sdk.authenticate = function (option) {
		window.location.href = sdk.host + option.path;
	};

	sdk.disconnect = function () {
		sendEventDownstream('disconnect');
	};

	sdk.logout = function() {
	    sendEventDownstream('logout');
	}

	sdk.service = function (method, params) {
		sendEventDownstream('service', { method: method, params: params});
	};

	window.ZwoopCustomerApiSdk = sdk;

})(window);
